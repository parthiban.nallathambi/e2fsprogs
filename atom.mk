LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_HOST_MODULE := e2fsprogs
LOCAL_DESCRIPTION := e2fsprogs for image building
LOCAL_COPY_FILES := contrib/populate-extfs.sh:bin/

include $(BUILD_AUTOTOOLS)

